package ui;

import Map.Floor;
import Map.Wall;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.stage.Stage;
import sprites.Enemy;
import sprites.Player;
import world.TankWorld;

public class LevelManager {
	
	
	public Scene missionStart(TankWorld world, int level) {
		BorderPane border = new BorderPane();
		Scene scene = new Scene(border, 400, 400);
		Label missionNum = new Label("Mission " + level);
		Label enemyNum = new Label("Enemy Tanks: " + world.getObjects(Enemy.class).size());
		VBox box = new VBox();
		missionNum.setAlignment(Pos.CENTER);
		missionNum.setFont(Font.font(50));

		missionNum.setTextFill(Color.BURLYWOOD);
		missionNum.setStyle("-fx-font-weight: bold");
		
		enemyNum.setAlignment(Pos.CENTER);
		enemyNum.setFont(Font.font(30));
		
		enemyNum.setTextFill(Color.CRIMSON);
		missionNum.setStyle("-fx-font-weight: bold");
		
		box.getChildren().addAll(missionNum, enemyNum);
		box.setAlignment(Pos.CENTER);
		border.setCenter(box);
		return scene;
	}

	public Scene mission1(BorderPane border, TankWorld tankWrld, Stage stage, Player tank) {
		
		Floor floorV = new Floor();
		floorV.setFitHeight(stage.getHeight());
		floorV.setFitWidth(stage.getWidth());
		tankWrld.add(floorV);
		floorV.setFitHeight(stage.getHeight());
		floorV.setFitWidth(stage.getWidth());

		tank = new Player(0);

		tank.setX(50);
		tank.setY(650);

		tank.setFitHeight(30);
		tank.setFitWidth(30);
		floorV.setFitWidth(stage.getWidth());

		for(int h = 1; h < 19; h++) {
			Wall wall = new Wall();
			wall.setX(h*wall.getWidth());
			wall.setY(100 + wall.getHeight());
			tankWrld.add(wall);
		}
		for (int i = 7; i < 18; i++) {
			Wall wall = new Wall();
			wall.setX(340 + wall.getWidth());
			wall.setY(i * wall.getHeight());
			tankWrld.add(wall);
		}
		for (int i = 17; i < 28; i++) {
			Wall wall = new Wall();
			wall.setX(320 + wall.getWidth());
			wall.setY(i * wall.getHeight());
			tankWrld.add(wall);
		}
		for (int i = 17; i < 33; i++) {
			Wall wall = new Wall();
			wall.setX(i * wall.getWidth());
			wall.setY(540 + wall.getHeight());
			tankWrld.add(wall);
		}
		
		Enemy enem1 = new Enemy(90);
		enem1.setX(100);
		enem1.setY(50);
		enem1.setFitHeight(30);
		enem1.setFitWidth(30);
		tankWrld.add(enem1);

		Enemy enem2 = new Enemy(180);
		enem2.setX(650);
		enem2.setY(tank.getY());
		enem2.setFitHeight(30);
		enem2.setFitWidth(30);
		tankWrld.add(enem2);

		Enemy enem3 = new Enemy(270);
		enem3.setX(600);
		enem3.setY(650);
		enem3.setFitHeight(30);
		enem3.setFitWidth(30);
		tankWrld.add(enem3);
		tankWrld.add(tank);
		
		border = new BorderPane();
		
		border.setCenter(tankWrld);
		
		Scene scene = new Scene(border, 750, 750);
		return scene;
	}
	
	public void mission2(BorderPane border, TankWorld tankWrld, Stage stage, Player tank) {

		
		Floor floor = new Floor();
		floor.setFitHeight(stage.getHeight());
		floor.setFitWidth(stage.getWidth());
		tankWrld.add(floor);
		floor.setFitHeight(stage.getHeight());
		floor.setFitWidth(stage.getWidth());
		
		for (int i = 7; i < 14; i++) {
			Wall wall = new Wall();
			wall.setX(100 + wall.getWidth());
			wall.setY(i * wall.getHeight());
			tankWrld.add(wall);
		}
		for (int i = 13; i < 20; i++) {
			Wall wall = new Wall();
			wall.setX(120 + wall.getWidth());
			wall.setY(i * wall.getHeight());
			tankWrld.add(wall);
		}
		
		for (int i = 13; i < 20; i++) {
			Wall wall = new Wall();
			wall.setX(140 + wall.getWidth());
			wall.setY(i * wall.getHeight());
			tankWrld.add(wall);
		}
		
		for (int i = 19; i < 26; i++) {
			Wall wall = new Wall();
			wall.setX(100 + wall.getWidth());
			wall.setY(i * wall.getHeight());
			tankWrld.add(wall);
		}
		for (int i = 15; i < 22; i++) {
			Wall wall = new Wall();
			wall.setX(i * wall.getWidth());
			wall.setY(50 + wall.getHeight());
			tankWrld.add(wall);
		}
		for (int i = 15; i < 22; i++) {
			Wall wall = new Wall();
			wall.setX(i * wall.getWidth());
			wall.setY(550 + wall.getHeight());
			tankWrld.add(wall);
		}
		
		for (int i = 7; i < 14; i++) {
			Wall wall = new Wall();
			wall.setX(590 + wall.getWidth());
			wall.setY(i * wall.getHeight());
			tankWrld.add(wall);
		}
		for (int i = 13; i < 20; i++) {
			Wall wall = new Wall();
			wall.setX(550 + wall.getWidth());
			wall.setY(i * wall.getHeight());
			tankWrld.add(wall);
		}
		
		for (int i = 13; i < 20; i++) {
			Wall wall = new Wall();
			wall.setX(570 + wall.getWidth());
			wall.setY(i * wall.getHeight());
			tankWrld.add(wall);
		}
		
		for (int i = 19; i < 26; i++) {
			Wall wall = new Wall();
			wall.setX(590 + wall.getWidth());
			wall.setY(i * wall.getHeight());
			tankWrld.add(wall);
		}
		
		Enemy enem4 = new Enemy(270);
		enem4.setX(450);
		enem4.setY(550);
		enem4.setFitHeight(30);
		enem4.setFitWidth(30);
		tankWrld.add(enem4);
		
		Enemy enem5 = new Enemy(90);
		enem5.setX(490);
		enem5.setY(100);
		enem5.setFitHeight(30);
		enem5.setFitWidth(30);
		tankWrld.add(enem5);
		
		Enemy enem6 = new Enemy(180);
		enem6.setX(675);
		enem6.setY(75);
		enem6.setFitHeight(30);
		enem6.setFitWidth(30);
		tankWrld.add(enem6);
		
		Enemy enem7 = new Enemy(180);
		enem7.setX(650);
		enem7.setY(325);
		enem7.setFitHeight(30);
		enem7.setFitWidth(30);
		tankWrld.add(enem7);
		
		Enemy enem8 = new Enemy(180);
		enem8.setX(675);
		enem8.setY(650);
		enem8.setFitHeight(30);
		enem8.setFitWidth(30);
		tankWrld.add(enem8);
		
		tank = new Player(0);
		tank.setX(35);
		tank.setY(325);
		tank.setFitHeight(30);
		tank.setFitWidth(30);
		tankWrld.add(tank);
		
		border = new BorderPane();
		
		border.setCenter(tankWrld);
		
		Scene scene = new Scene(border, 750, 750);
		stage.setScene(scene);
		
		tankWrld.start();
		tankWrld.requestFocus();

	}

	public Scene mission3() {
		return null;

	}
	public Scene mission4() {
		return null;

	}
	public Scene mission5() {
		return null;

	}
	public Scene mission6() {
		return null;

	}
	public Scene mission7() {
		return null;

	}
	public Scene mission8() {
		return null;

	}
	public Scene mission9() {
		return null;

	}
	public Scene mission10() {
		return null;

	}
	
	public Scene initLevel(BorderPane border, TankWorld tankWrld, Stage stage, Player tank, int level) {
		Scene scene = null;
		
		switch(level) {
		case 1:
			scene = mission1(border, tankWrld, stage, tank);
			return scene;
		case 2:
			mission2(border, tankWrld, stage, tank);
			
		default:
			return scene;
		} 
		
	}

}



