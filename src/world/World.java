 package world;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import sprites.Actor;
import javafx.collections.ObservableList;
import javafx.animation.AnimationTimer;
import javafx.animation.PauseTransition;
import javafx.scene.Node;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.Pane;
import javafx.util.Duration;

public abstract class World extends Pane {
	private AnimationTimer timer;
	private HashSet<KeyCode> h = new HashSet<>();

	public World() {
		timer = new AnimTimer();
	}
	public abstract void act(long now);

	public void add(Actor actor) {
		getChildren().add(actor);
	}


	public void addKeyCode(KeyCode key) {
		h.add(key);
	}

	public void removeKeyCode(KeyCode key) {
		h.remove(key);
	}

	public boolean isKeyDown(KeyCode key) {
		for (int i = 0; i < h.size(); i++) {
			if (h.contains(key)) {
				return true;
			}
		}
		return false;
	}

	@SuppressWarnings("unchecked")
	public <A extends Actor> java.util.List<A> getObjects(java.lang.Class<A> cls) {
		ArrayList<A> temp = new ArrayList<>();
		ObservableList<Node> a = getChildren();
		for (int i = 0; i < a.size(); i++) {
			if (cls.isAssignableFrom(a.get(i).getClass())) {
				temp.add((A) a.get(i));
			}
		}
		return temp;
	}

	public void remove(Actor actor) {
		getChildren().remove(actor);
	}

	public void stop() {
		timer.stop();
	}

	public void start() {
		PauseTransition pause = new PauseTransition(Duration.seconds(3));
		pause.setOnFinished(event -> 
		timer.start()
				);
		pause.play();

	}


	private class AnimTimer extends AnimationTimer {

		@Override
		public void handle(long e) {
			act(e);
			List<Actor> list = getObjects(Actor.class);
			for (int i = 0; i < list.size(); i++) {
				list.get(i).act(e);
			}
		}

	}




}
