package sprites;

import java.net.URISyntaxException;
import java.util.ArrayList;
import Map.Wall;
import javafx.scene.image.Image;
import javafx.scene.input.KeyCode;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;

public class Player extends Actor {
	private int delay;
	private final String UP = "UP";
	private final String DOWN = "DOWN";
	private final String LEFT = "LEFT";
	private final String RIGHT = "RIGHT";	
	public boolean destroyed;
	

//	private
	public Player(double direction) {
		String path =  getClass().getClassLoader().getResource("resources/player.png").toString();
		Image image = new Image(path);
		this.setImage(image);
		//delay = 60;
		this.setRotate(direction);
		this.setFitHeight(30);
		this.setFitWidth(30);
	}
	//
	
	
	public void act(long now) {
		
		
		if (!hasWalls(UP) && this.getY() > 0 && getWorld().isKeyDown(KeyCode.W)) {
			this.setY(this.getY() - 3);
		}
		if (!hasWalls(DOWN) && this.getY() + this.getFitHeight() < getWorld().getHeight() && getWorld().isKeyDown(KeyCode.S)) {
			this.setY(this.getY() + 3);
		}
		if (!hasWalls(LEFT) && this.getX() > 0 && getWorld().isKeyDown(KeyCode.A)) {
			this.setX(this.getX() - 3);
		}
		if (!hasWalls(RIGHT) && this.getX() + this.getFitWidth() < getWorld().getWidth() && getWorld().isKeyDown(KeyCode.D)) {
			this.setX(this.getX() + 3);
		}
		
		if (delay >= 60 && getWorld().isKeyDown(KeyCode.SPACE)) {
			delay = 0;
			Bullets temp = new Bullets(this, getWorld());
			temp.setFitHeight(15);
			temp.setFitWidth(15);
			delay = 0;
			String s = null;
			try {
				s = getClass().getClassLoader().getResource("resources/shoot.wav").toURI().toString();
			} catch (URISyntaxException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			Media media = new Media(s);
			MediaPlayer mediaPlayer = new MediaPlayer(media);
			mediaPlayer.play();
				
		}
		delay++;
		if(this.getIntersectingObjects(EnemyBullets.class).size() > 0) {
			this.getWorld().remove(this);
			
		}else if(this.getIntersectingObjects(Mines.class).size() > 0) {
			this.getWorld().remove(this);
			
		}
	}
	public boolean hasWalls(String dir) {
		Wall tempWall = new Wall();
		if(this.getIntersectingObjects(Wall.class).size() == 0) {
			return false;
		}else {
			if(dir == UP) {
				return getWallObjectsAt(this.getX() + this.getFitWidth() / 2, this.getY() - 3*tempWall.getFitHeight()/4).size() > 0 ||
						getWallObjectsAt(this.getX() + this.getFitWidth() - 5, this.getY() - 3*tempWall.getFitHeight()/4).size() > 0 ||
						getWallObjectsAt(this.getX() + 5, this.getY() - 3*tempWall.getHeight()/4).size() > 0;
			}
			if(dir == DOWN) {
				return getWallObjectsAt(this.getX() + this.getFitWidth() / 2, this.getY() + this.getFitHeight() + 3*tempWall.getFitHeight()/4).size() > 0||
						getWallObjectsAt(this.getX() + this.getFitWidth() - 5, this.getY() + this.getFitHeight() + 3*tempWall.getFitHeight()/4).size() > 0 ||
						getWallObjectsAt(this.getX() + 5, this.getY() + this.getFitHeight() + 3*tempWall.getFitHeight()/4).size() > 0;
			}
			if(dir == LEFT) {
				return getWallObjectsAt(this.getX() - 3*tempWall.getFitWidth()/4, this.getY() + this.getFitHeight()/2).size() > 0||
						getWallObjectsAt(this.getX() - 3*tempWall.getFitWidth()/4, this.getY() + 5).size() > 0 ||
						getWallObjectsAt(this.getX() - 3*tempWall.getFitWidth()/4, this.getY() + this.getFitHeight() - 5).size() > 0;
			}
			if(dir == RIGHT) {
				
				return getWallObjectsAt(this.getX() + this.getFitWidth() + 3*tempWall.getFitWidth()/4, this.getY() + this.getFitHeight()/2).size() > 0||
						getWallObjectsAt(this.getX() + this.getFitWidth() + 3*tempWall.getFitWidth()/4, this.getY() + 5).size() > 0 ||
						getWallObjectsAt(this.getX() + this.getFitWidth() + 3*tempWall.getFitWidth()/4, this.getY() + this.getFitHeight() - 5).size() > 0;				
			}
			return true;
		}
		
		
	}		
	
//	public boolean getPlayer() {
//		return isPlayer;
//	}
	
	
	private ArrayList<Wall> getWallObjectsAt(double xPos, double yPos){
		boolean minX = false;
		boolean maxX = false;
		boolean minY = false;
		boolean maxY = false;
		Wall tempWall = new Wall();
		ArrayList<Wall> walle = new ArrayList<Wall>();
		for(int i = 0; i < this.getIntersectingObjects(Wall.class).size(); i++) {
			minX = false;
			maxX = false;
			minY = false;
			maxY = false;
			if(this.getIntersectingObjects(Wall.class).get(i).getX() <= xPos) {
				
				minX = true;
			}
//			System.out.print("minX = " + minX + "	");
			if(this.getIntersectingObjects(Wall.class).get(i).getX() + tempWall.getWidth() >= xPos) {
				
				maxX = true;
			}
//			System.out.print("maxX = " + maxX + "	");
			if(this.getIntersectingObjects(Wall.class).get(i).getY() <= yPos) {
				
				minY = true;
			}
//			System.out.print("minY = " + minY + "	");
			if(this.getIntersectingObjects(Wall.class).get(i).getY() + tempWall.getHeight() >= yPos) {
				
				maxY = true;
			}
//			System.out.println("maxY = " + maxY + "	");
			if(minX && maxX && minY && maxY) {
				walle.add(this.getIntersectingObjects(Wall.class).get(i));
//				System.out.println(walle);
				return walle;
			}
		}
//		System.out.println(walle);
		return walle;
	}
	

}

