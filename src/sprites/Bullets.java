package sprites;


import Map.Wall;
import javafx.scene.image.Image;
import world.World;

public class Bullets extends Actor{
	private double Dx;
	private double Dy;
	private int speed;
	private double direction;
	private boolean destroyed;
	private int bounces;
	
	public Bullets(Player tank, World tankWorld) {
		String path =  getClass().getClassLoader().getResource("resources/bullet (3).png").toString();
		System.out.println(path);
		Image temp = new Image(path);
		

		direction = (Math.PI* tank.getRotate()) / 180;
		//direction = tank.getRotate();
		speed = 10;
		this.setRotate(direction);
		Dx = speed * Math.cos(direction);
		Dy = speed * Math.sin(direction);
		this.setImage(temp);
		this.setFitHeight(20);
		this.setFitWidth(20);
		this.setX(tank.getX() + 20);
		this.setY(tank.getY() + 20);
		this.bounces = 0;
		destroyed = false;
		tankWorld.add(this);
	}
	
	
	/*public void setSpeedX(int speed) {
		this.speed = speed;
	}
	
	public void changeDX(double a) {
		Dx = a;
	}
	
	public void changeDY(double a) {
		Dy = a;
	}*/
	
	public boolean isDestroyed() {
		return destroyed;
	}

	@Override
	public void act(long now) {
		if(!destroyed) {
			this.setRotate(direction);
			Dx = speed * Math.cos(direction);
			Dy = speed * Math.sin(direction);
		
			move(Dx, Dy);
			if (this.getY() > getWorld().getHeight() - 10) {
				this.direction = -this.direction;
				bounces++;
			}
			if (this.getX() > getWorld().getWidth() - 15) {
				this.direction = Math.PI - direction;
				bounces++;
			}
			if (this.getY() < 0) {
				this.direction = -this.direction;
				bounces++;
			}//
			if (this.getX() < 0) {
				this.direction = Math.PI - direction;
				bounces++;
			}
			if(this.getIntersectingObjects(Mines.class).size() > 0 || this.getIntersectingObjects(Wall.class).size() > 0) {
				destroyed = true;
			}
			if(bounces > 1) {
				destroyed = true;
			}
			if(this.getIntersectingObjects(EnemyBullets.class).size() > 0) {
				destroyed = true;
			}
			if (this.getIntersectingObjects(Enemy.class).size() > 0) {
				destroyed = true;
			}
		
		}else {
			getWorld().remove(this);
		}
	}
}