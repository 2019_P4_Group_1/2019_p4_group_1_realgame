package sprites;
import java.util.*;

import world.World;
import javafx.geometry.Bounds;
import javafx.scene.image.ImageView;

public abstract class Actor extends ImageView {
	
	public abstract void act(long now);
	
	public double getHeight() {
				
		Bounds b = getBoundsInParent();
		return b.getHeight();
	}
	
	public double getWidth() {
		Bounds b = getBoundsInParent();
		return b.getWidth();
	}
	
	public World getWorld() {
		return (World) getParent();
	}
	
	public void move(double dx, double dy) {
		double xPos = this.getX();
		double yPos = this.getY();
		this.setX(xPos + dx);
		this.setY(yPos + dy);
	}
	
	
	public <A extends Actor> java.util.List<A> getIntersectingObjects(java.lang.Class<A> cls){
		List<A> objs = getWorld().getObjects(cls);
		ArrayList<A> inOb = new ArrayList<A>();
		for(int i = 0; i < objs.size(); i++) {
			if(objs.get(i).intersects(getBoundsInLocal())) {
				inOb.add(objs.get(i));
			}
		}
		return inOb;
	}
	
	public <A extends Actor> A getOneIntersectingObject(java.lang.Class<A> cls) {
		List<A> objs = getWorld().getObjects(cls);
		ArrayList<A> inOb = new ArrayList<A>();
		for(int i = 0; i < objs.size(); i++) {
			if(objs.get(i).intersects(getBoundsInLocal())) {
				inOb.add(objs.get(i));
				return inOb.get(0);
			}
		}
		return null;
	}
}
