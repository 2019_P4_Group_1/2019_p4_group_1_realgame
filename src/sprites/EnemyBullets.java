package sprites;
//import java.util.Timer;
import world.World;

import Map.Wall;
import javafx.scene.image.Image;

public class EnemyBullets extends Actor{
	private double Dx;
	private double Dy;
	private int speed;
	private double direction;
	private int bounces;
	private boolean destroyed;
	public EnemyBullets(Enemy tank, World tankWorld, Player player) {
		String path = getClass().getClassLoader().getResource("resources/bullet (3) copy.png").toString();
		Image temp = new Image(path);
		direction = (Math.PI* tank.getRotate()) / 180;
		//direction = tank.getRotate();
		speed = 4;
		this.setRotate(direction);
		Dx = speed * Math.cos(direction);
		Dy = speed * Math.sin(direction);
		this.setImage(temp);
		this.setFitHeight(20);
		this.setFitWidth(20);
		this.setX(tank.getX() + 20);
		this.setY(tank.getY() + 20);
		this.bounces = 0;
		destroyed = false;
		tankWorld.add(this);
	}
	


	/*public void update() {
		Dx += speed;
		
	}
	
	public void setSpeedX(int speed) {
		this.speed = speed;
	}
	
	public void changeDX(double a) {

		Dx = a;
	}
	
	public void changeDY(double a) {
		Dy = a;
	}
	
	public boolean isDestroyed() {
		return destroyed;
	}*/
//
	@Override
	public void act(long now) {
		if(!destroyed) {
			this.setRotate(direction);
			Dx = speed * Math.cos(direction);
			Dy = speed * Math.sin(direction);
		
			move(Dx, Dy);
			if (this.getY() > getWorld().getHeight() - 10) {
				this.direction = -this.direction;
				bounces++;
			}
			if (this.getX() > getWorld().getWidth() - 15) {
				this.direction = Math.PI - direction;
				bounces++;
			}
			if (this.getY() < 0) {
				this.direction = -this.direction;
				bounces++;
			}
			if (this.getX() < 0) {
				this.direction = Math.PI - direction;
				bounces++;
			}
			if(this.getIntersectingObjects(Mines.class).size() > 0 ||
					this.getIntersectingObjects(Player.class).size() > 0 ||
					 this.getIntersectingObjects(Wall.class).size() > 0) {
				destroyed = true;
			}
			if (this.getIntersectingObjects(Wall.class).size() > 0) {
				this.direction = Math.PI - direction;
				bounces++;
			}
			if(bounces > 1) {
				destroyed = true;
			}
			if(this.getIntersectingObjects(Bullets.class).size() > 0) {
				destroyed = true;
			}
		}else {
			getWorld().remove(this);
		}
	}
//	public void pointTo(Player pl) {
//		Point2D tPt = new Point2D(tank.getX(), tank.getY());
//		double mouseX = e.getSceneX();
//		double mouseY = e.getSceneY();
//		double tankX = tank.getX();
//		double tankY = tank.getY();
//		if(tankX >= mouseX) {
//			double tAng = Math.atan((mouseY - tankY)/(mouseX - tankX));
//			tAng = (tAng * 180)/ Math.PI +180;
//			tank.setRotate(tAng);
//		}else {
//			double tAng = Math.atan((mouseY - tankY)/(mouseX - tankX));
//			tAng = (tAng * 180)/ Math.PI;
//			tank.setRotate(tAng);
//		}
//
//	}
}
