package sprites;

import java.net.URISyntaxException;
import java.util.Random;
import Map.Wall;
import javafx.scene.image.Image;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;


public class Enemy extends Actor{
	private int delay;
	Random rand = new Random();
	private double Dx, Dy;
	private double direction;
	private double speed;
	private final String UP = "UP";
	private final String DOWN = "DOWN";
	private final String LEFT = "LEFT";
	private final String RIGHT = "RIGHT";
	private boolean destroyed;
	
	public Enemy(double direction) {
		String path =  getClass().getClassLoader().getResource("resources/enemy.png").toString();
		Image temp = new Image(path);
		this.setImage(temp);
		delay = 60;
		Dx = rand.nextInt(2) - 1;
		Dy = rand.nextInt(2) - 1;
		this.setRotate(direction);
		this.setFitHeight(30);
		this.setFitWidth(30);
		speed = 1;
		destroyed = false;
	}

	@Override
	public void act(long now) {
		//atEdge = false;
		
		
		if(!destroyed) {
			Dx = speed * Math.cos(direction);
			Dy = speed * Math.sin(direction);
			move(Dx, Dy);
			rotate(this);
			if (this.getY() > getWorld().getHeight() - this.getFitHeight()) {
				this.direction = 180 + this.direction;
			}
			if (this.getX() > getWorld().getWidth() - this.getFitWidth()) {
				this.direction = 180 + direction;
			}
			if (this.getY() < 0) {
				this.direction = -this.direction;
			}
			if (this.getX() < 0) {
				this.direction = 180 + direction;
			}
			if (this.getIntersectingObjects(Wall.class).size() > 0) {
				this.direction += 180;
			}
			if (this.getIntersectingObjects(Bullets.class).size() > 0 || this.getIntersectingObjects(Mines.class).size() > 0) {
				destroyed = true;
			}
			if(delay >= 120) {			
				if(getWorld().getObjects(Player.class).size() > 0) {
					EnemyBullets temp = new EnemyBullets(this, getWorld(), getWorld().getObjects(Player.class).get(0));
					temp.setFitHeight(15);
					temp.setFitWidth(15);
				}
				String s = null;
				try {
					s = getClass().getClassLoader().getResource("resources/shoot.wav").toURI().toString();
				} catch (URISyntaxException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				Media media = new Media(s);
				MediaPlayer mediaPlayer = new MediaPlayer(media);
				mediaPlayer.play();

				delay = 0;
			}
			delay++;
		}else {
			getWorld().remove(this);
		}
	}
	

	public boolean checkWalls(String dir) {
		if(this.getIntersectingObjects(Wall.class).size() == 0) {
			return false;
		}else {
			if(dir == UP) {
				return (this.getIntersectingObjects(Wall.class).get(0).getY() <= this.getY());
			}
			if(dir == DOWN) {
				return (this.getIntersectingObjects(Wall.class).get(0).getY() >= this.getY() + this.getFitHeight());
			}
			if(dir == LEFT) { 
				return (this.getIntersectingObjects(Wall.class).get(0).getX() <= this.getX());
			}
			if(dir == RIGHT){
				return (this.getIntersectingObjects(Wall.class).get(0).getX() >= this.getX() + this.getFitWidth());
			}
			return false;
		}
	}
	
	public void rotate(Enemy a) {
		if(getWorld().getObjects(Player.class).size() > 0) {
			double mouseX = getWorld().getObjects(Player.class).get(0).getX();
			double mouseY = getWorld().getObjects(Player.class).get(0).getY();
			double tankX = a.getX();
			double tankY = a.getY();				
			if(tankX >= mouseX) {
				double tAng = Math.atan((mouseY - tankY)/(mouseX - tankX));
				tAng = (tAng * 180)/ Math.PI +180;
				a.setRotate(tAng);
			}else {
				double tAng = Math.atan((mouseY - tankY)/(mouseX - tankX));
				tAng = (tAng * 180)/ Math.PI;
				a.setRotate(tAng);
			}
		}
	}
	
	public double getTan(Enemy a) {
		if(getWorld().getObjects(Player.class).size() > 0) {
			double mouseX = getWorld().getObjects(Player.class).get(0).getX();
			double mouseY = getWorld().getObjects(Player.class).get(0).getY();
			double tankX = a.getX();
			double tankY = a.getY();				
			if(tankX >= mouseX) {
				double tAng = Math.atan((mouseY - tankY)/(mouseX - tankX));
				return tAng;
			}else {
				double tAng = Math.atan((mouseY - tankY)/(mouseX - tankX));
				return tAng;
			}
		}
		return -1;
	}
	
}