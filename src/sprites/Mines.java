package sprites;
import java.net.URISyntaxException;
import javafx.scene.image.Image;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;

public class Mines extends Actor{

	private int state = 0;
	private boolean startCount;
	private int count;
	
	public Mines(int s) {
		String path =  getClass().getClassLoader().getResource("resources/mine.png").toString();
		state = s;
		startCount = false;
		count = 0;
		Image image = new Image(path);
		this.setImage(image);
		this.setFitHeight(20);
		this.setFitWidth(20);

	}

	public int getState() {
		return state;
	}

	@Override
	public void act(long now) {
		//System.out.println(this.getIntersectingObjects(Enemy.class).size());
		if (!startCount && this.getIntersectingObjects(Enemy.class).size() > 0) {
			state = 1;
			this.setImage(new Image("explosion.png"));
			startCount = true;
			String s = null;
			try {
				s = getClass().getClassLoader().getResource("resources/Bomb.mp3").toURI().toString();
			} catch (URISyntaxException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			Media media = new Media(s);
			MediaPlayer mediaPlayer = new MediaPlayer(media);
			mediaPlayer.play();
			
		}
		if (!startCount && this.getIntersectingObjects(Bullets.class).size() > 0) {
			state = 1;
			this.setImage(new Image("explosion.png"));
			startCount = true;
			String s = null;
			try {
				s = getClass().getClassLoader().getResource("resources/Bomb.mp3").toURI().toString();
			} catch (URISyntaxException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			Media media = new Media(s);
			MediaPlayer mediaPlayer = new MediaPlayer(media);
			mediaPlayer.play();
		}
		if (!startCount && this.getIntersectingObjects(EnemyBullets.class).size() > 0) {
			state = 1;
			this.setImage(new Image("explosion.png"));
			startCount = true;
			String s = null;
			try {
				s = getClass().getClassLoader().getResource("resources/Bomb.mp3").toURI().toString();
			} catch (URISyntaxException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			Media media = new Media(s);
			MediaPlayer mediaPlayer = new MediaPlayer(media);
			mediaPlayer.play();
		}
		if (!startCount && this.getIntersectingObjects(Player.class).size() > 0) {
			state = 1;
			this.setImage(new Image("explosion.png"));
			startCount = true;
			String s = null;
			try {
				s = getClass().getClassLoader().getResource("resources/Bomb.mp3").toURI().toString();
			} catch (URISyntaxException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			Media media = new Media(s);
			MediaPlayer mediaPlayer = new MediaPlayer(media);
			mediaPlayer.play();
			
		}
		
		if(startCount) {
			if(count < 45) {
				count++;
			}else {
				count = 0;
				getWorld().remove(this);
			}

		}


	}



}
