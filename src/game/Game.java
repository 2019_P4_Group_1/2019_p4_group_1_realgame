
package game;


import java.io.InputStream;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import Map.Floor;
import Map.Wall;
import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.PauseTransition;
import javafx.animation.Timeline;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Point2D;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.scene.media.MediaView;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.util.Duration;
import sprites.Enemy;
import sprites.Mines;
import sprites.Player;
import ui.LevelManager;
import world.TankWorld;

public class Game extends Application{

	BorderPane gamePane = new BorderPane();
	BorderPane titlePane = new BorderPane();
	BorderPane border3 = new BorderPane();
	BorderPane gamePane2;
	TankWorld tankWrld;
	boolean isOver;
	Player tank;
	final MediaView mediaView = new MediaView();
	List<String> list;
	Timeline checkStatus;
	private static int level;
	ArrayList<String> levels = new ArrayList<>();
	Button nextButton = new Button("Next Level");
	public static void main(String[] args) {
		level = 1;
		launch(args);
	}

	@Override
	public void start(Stage stage) throws Exception {

		stage.setTitle("We Plei 10QS");

		LevelManager missions = new LevelManager();

		levels.add("mission1.txt");
		levels.add("mission2.txt");
		levels.add("mission3.txt");
		levels.add("mission4.txt");
		levels.add("mission5.txt");
		levels.add("mission6.txt");


		System.out.println(level);
		tankWrld = new TankWorld();

		list = new ArrayList<String>();
		list.add("resources/RoundStart.mp3");
		list.add("resources/Variation1.mp3");
		list.add("resources/RoundFailed.mp3");
		list.add("resources/GameWon.mp3");
		list.add("resources/Variation5.mp3");
		list.add("resources/AllVariations.mp3");

		gamePane = new BorderPane();
		titlePane = new BorderPane();
		Button startButton;
		Scene startScene = new Scene(titlePane, 400, 350, Color.WHITE);
		Scene gameScene = new Scene(gamePane, 750, 750);
		if(level == 1) {
			startButton = new Button("PLAY");
			stage.setWidth(750);
			stage.setHeight(750);

			//Start Scene
			Label title1 = new Label("We Plei 10QS");
			title1.setFont(new Font(20));
			title1.setTranslateX(startScene.getWidth() / 2 - 60);

			VBox buttons = new VBox();

			buttons.setPadding(new Insets(15, 20, 15, 12));
			buttons.setSpacing(100);



			Floor floor = new Floor();
			floor.setFitHeight(stage.getHeight());
			floor.setFitWidth(stage.getWidth());
			tankWrld.add(floor);
			floor.setFitHeight(stage.getHeight());
			floor.setFitWidth(stage.getWidth());
			tank = new Player(0);

			InputStream stream = getClass().getClassLoader().getResourceAsStream("resources/" + levels.get(level - 1));
			Scanner scan = new Scanner(stream);

			while (scan.hasNext()) {
				String obj = scan.nextLine();
				Scanner lnScan = new Scanner(obj);
				String cls = lnScan.next();
				double x = lnScan.nextDouble();
				double y = lnScan.nextDouble();
				System.out.println("cls = " + cls + "	x = " + x + "	y = " + y);
				lnScan.close();
				//scan.nextLine();
				if (cls.equals("Wall")) {
					Wall wall = new Wall();
					wall.setX(x);
					wall.setY(y);
					tankWrld.add(wall);
				} else if (cls.equals("Enemy")) {
					Enemy enem = new Enemy(0);
					enem.setX(x);
					enem.setY(y);
					tankWrld.add(enem);
				} else if (cls.equals("Player")) {

					tank.setX(x);
					tank.setY(y);
					tankWrld.add(tank);
				} else if (cls.equals("Mines")) {
					Mines m = new Mines(0);
					m.setX(x);
					m.setY(y);
					tankWrld.add(m);
				}
			}
			scan.close();
			Text text = new Text();
			text.setFont(new Font(16));
			text.setTranslateX(startScene.getWidth() / 2 - 195);
			text.setTextAlignment(TextAlignment.CENTER);
			text.setText("WASD to move around\n\nSpace to shoot\n\nMouse to aim");

			buttons.getChildren().addAll(text, startButton);
			buttons.setAlignment(Pos.CENTER);

			titlePane.setTop(title1);
			titlePane.setCenter(buttons);
		}else if(level > 1 && level < 7) {
			startButton = new Button("NEXT LEVEL");
			stage.setWidth(750);
			stage.setHeight(750);

			//Start Scene
			Label title1 = new Label("We Plei 10QS");
			title1.setFont(new Font(20));
			title1.setTranslateX(startScene.getWidth() / 2 - 60);

			VBox buttons = new VBox();

			buttons.setPadding(new Insets(15, 20, 15, 12));
			buttons.setSpacing(100);



			Floor floor = new Floor();
			floor.setFitHeight(stage.getHeight());
			floor.setFitWidth(stage.getWidth());
			tankWrld.add(floor);
			floor.setFitHeight(stage.getHeight());
			floor.setFitWidth(stage.getWidth());
			tank = new Player(0);

			InputStream stream = getClass().getClassLoader().getResourceAsStream("resources/" + levels.get(level - 1));
			Scanner scan = new Scanner(stream);
			//InputStream in = getClass().getResourceAsStream("mission1.txt");
			//Scanner scan = new Scanner(in);
			while (scan.hasNext()) {
				String obj = scan.nextLine();
				Scanner lnScan = new Scanner(obj);
				String cls = lnScan.next();
				double x = lnScan.nextDouble();
				double y = lnScan.nextDouble();
				System.out.println("cls = " + cls + "	x = " + x + "	y = " + y);
				lnScan.close();
				//scan.nextLine();
				if (cls.equals("Wall")) {
					Wall wall = new Wall();
					wall.setX(x);
					wall.setY(y);
					tankWrld.add(wall);
				} else if (cls.equals("Enemy")) {
					Enemy enem = new Enemy(0);
					enem.setX(x);
					enem.setY(y);
					tankWrld.add(enem);
				} else if (cls.equals("Player")) {

					tank.setX(x);
					tank.setY(y);
					tankWrld.add(tank);
				} else if (cls.equals("Mines")) {
					Mines m = new Mines(0);
					m.setX(x);
					m.setY(y);
					tankWrld.add(m);
				}
			}
			scan.close();
			Text text = new Text();
			text.setFont(new Font(16));
			text.setTranslateX(startScene.getWidth() / 2 - 195);
			text.setTextAlignment(TextAlignment.CENTER);
			text.setText("You won this mission!!");

			buttons.getChildren().addAll(text, startButton);
			buttons.setAlignment(Pos.CENTER);

			titlePane.setTop(title1);
			titlePane.setCenter(buttons);
		}else {
			level = 1;
			startButton = new Button("PLAY");
			stage.setWidth(750);
			stage.setHeight(750);

			//Start Scene
			Label title1 = new Label("We Plei 10QS");
			title1.setFont(new Font(20));
			title1.setTranslateX(startScene.getWidth() / 2 - 60);

			VBox buttons = new VBox();

			buttons.setPadding(new Insets(15, 20, 15, 12));
			buttons.setSpacing(100);



			Floor floor = new Floor();
			floor.setFitHeight(stage.getHeight());
			floor.setFitWidth(stage.getWidth());
			tankWrld.add(floor);
			floor.setFitHeight(stage.getHeight());
			floor.setFitWidth(stage.getWidth());
			tank = new Player(0);

			InputStream stream = getClass().getClassLoader().getResourceAsStream("resources/" + levels.get(level - 1));
			Scanner scan = new Scanner(stream);
			//InputStream in = getClass().getResourceAsStream("mission1.txt");
			//Scanner scan = new Scanner(in);
			while (scan.hasNext()) {
				String obj = scan.nextLine();
				Scanner lnScan = new Scanner(obj);
				String cls = lnScan.next();
				double x = lnScan.nextDouble();
				double y = lnScan.nextDouble();
				System.out.println("cls = " + cls + "	x = " + x + "	y = " + y);
				lnScan.close();
				//scan.nextLine();
				if (cls.equals("Wall")) {
					Wall wall = new Wall();
					wall.setX(x);
					wall.setY(y);
					tankWrld.add(wall);
				} else if (cls.equals("Enemy")) {
					Enemy enem = new Enemy(0);
					enem.setX(x);
					enem.setY(y);
					tankWrld.add(enem);
				} else if (cls.equals("Player")) {

					tank.setX(x);
					tank.setY(y);
					tankWrld.add(tank);
				} else if (cls.equals("Mines")) {
					Mines m = new Mines(0);
					m.setX(x);
					m.setY(y);
					tankWrld.add(m);
				}
			}
			scan.close();
			Text text = new Text();
			text.setFont(new Font(16));
			text.setTranslateX(startScene.getWidth() / 2 - 195);
			text.setTextAlignment(TextAlignment.CENTER);
			text.setText("WASD to move around\\n\\nSpace to shoot\\n\\nMouse to aim");

			buttons.getChildren().addAll(text, startButton);
			buttons.setAlignment(Pos.CENTER);

			titlePane.setTop(title1);
			titlePane.setCenter(buttons);
		}
		//		

		startButton.setOnAction((ActionEvent event) -> {
			Scene scene = missions.missionStart(tankWrld, level);
			stage.setWidth(750);
			stage.setHeight(750);
			//			System.out.println(scene.getWidth() + "," + scene.getHeight());
			stage.setScene(scene);
			play(0);

			PauseTransition wait = new PauseTransition(Duration.seconds(2));
			wait.setOnFinished((e) -> {
				play(4);
				gamePane.setCenter(tankWrld);
				gamePane.prefHeightProperty().bind(gameScene.heightProperty());
				gamePane.prefWidthProperty().bind(gameScene.widthProperty());
				/*
				 * Scene scene = mission1.initLevel();
				 * scene.setRoot(border); //this will be the first mission. I realize this code is kinda messy. Could have been organized better
				 * but it is what it is.
				 */
				stage.setWidth(gameScene.getWidth());
				stage.setHeight(gameScene.getHeight());
				//				System.out.println(gameScene.getWidth() + "," + gameScene.getHeight());
				stage.setScene(gameScene);	
				stage.sizeToScene();
				tankWrld.start();
				tankWrld.requestFocus();

			});
			wait.play();
		});



		checkStatus = new Timeline(new KeyFrame(Duration.millis(10), new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent e) {
				if (tankWrld.getObjects(Player.class).size() == 0) {
					checkStatus.stop();
					tankWrld.stop();
					pause(4);
					play(2);
					Stage window = new Stage();

					window.initModality(Modality.APPLICATION_MODAL);
					window.setTitle("Game Over!");
					window.setMinWidth(250);

					Label label = new Label("GAME OVER");
					label.setFont(Font.font(20));
					label.setStyle("-fx-font-weight: bold");

					Button mainMenu = new Button("MAIN MENU");
					mainMenu.setOnAction(event -> {
						window.close();
						stage.close();
						try {
							level = 1;
							start(stage);
						} catch (Exception e1) {

							e1.printStackTrace();
						}
					});
					VBox layout = new VBox(10);
					layout.getChildren().addAll(label, mainMenu);
					layout.setAlignment(Pos.CENTER);

					Scene scene = new Scene(layout, 250, 250);
					window.setScene(scene);
					window.setResizable(false);
					window.show();	

				} else if (tankWrld.getObjects(Enemy.class).size() == 0){
					checkStatus.stop();
					tankWrld.stop();
					pause(4);
					play(3);
					Stage window = new Stage();

					//						
					window.close();
					stage.close();
					try {
						level++;
						start(stage);
					} catch (Exception e1) {

						e1.printStackTrace();
					}

				}
			}
		}));

		checkStatus.setCycleCount(Animation.INDEFINITE);
		checkStatus.play();


		((BorderPane) gameScene.getRoot()).getChildren().add(mediaView);

		stage.setScene(startScene);
		stage.setWidth(startScene.getWidth());
		stage.setHeight(startScene.getHeight());

		stage.setResizable(false);

		stage.show();
		stage.sizeToScene();
		tankWrld.setOnKeyPressed(new EventHandler<KeyEvent> () {
			@Override
			public void handle(KeyEvent e) {
				if (e.getCode().equals(KeyCode.W)) {
					tankWrld.addKeyCode(KeyCode.W);
				}
				if (e.getCode().equals(KeyCode.S)) {
					tankWrld.addKeyCode(KeyCode.S);
				}
				if (e.getCode().equals(KeyCode.A)) {
					tankWrld.addKeyCode(KeyCode.A);
				}
				if (e.getCode().equals(KeyCode.D)) {
					tankWrld.addKeyCode(KeyCode.D);
				}
				if (e.getCode().equals(KeyCode.SPACE)) {
					tankWrld.addKeyCode(KeyCode.SPACE);
				}
			}

		});

		tankWrld.setOnKeyReleased(new EventHandler<KeyEvent> () {
			@Override
			public void handle(KeyEvent e) {
				if (e.getCode().equals(KeyCode.W)) {
					tankWrld.removeKeyCode(KeyCode.W);
				}
				if (e.getCode().equals(KeyCode.S)) {
					tankWrld.removeKeyCode(KeyCode.S);
				}
				if (e.getCode().equals(KeyCode.A)) {
					tankWrld.removeKeyCode(KeyCode.A);
				}
				if (e.getCode().equals(KeyCode.D)) {
					tankWrld.removeKeyCode(KeyCode.D);
				}
				if (e.getCode().equals(KeyCode.SPACE)) {
					tankWrld.removeKeyCode(KeyCode.SPACE);
				}

			}

		});



		class MovedHandler implements EventHandler<MouseEvent> {
			@SuppressWarnings("unused")
			double startX;
			@SuppressWarnings("unused")
			double startRotate;

			@Override
			public void handle(MouseEvent e) {
				//				Point2D tPt = new Point2D(tank.getX(), tank.getY());
				double mouseX = e.getSceneX();
				double mouseY = e.getSceneY();
				double tankX = tank.getX();
				double tankY = tank.getY();				
				if(tankX >= mouseX) {
					double tAng = Math.atan((mouseY - tankY)/(mouseX - tankX));
					tAng = (tAng * 180)/ Math.PI +180;
					tank.setRotate(tAng);
				}else {
					double tAng = Math.atan((mouseY - tankY)/(mouseX - tankX));
					tAng = (tAng * 180)/ Math.PI;
					tank.setRotate(tAng);
				}

			}

		}

		MovedHandler handler = new MovedHandler();

		tank.setOnMousePressed(e -> {
			Point2D pt = tank.localToParent(e.getX(), e.getY());
			handler.startX = pt.getX();
			handler.startRotate = tank.getRotate();
		});

		gameScene.setOnMouseMoved(handler);
	}
	public void play(int index) {

		String s = null;
		try {
			s = getClass().getClassLoader().getResource(list.get(index)).toURI().toString();
		} catch (URISyntaxException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		Media media = new Media(s);
		MediaPlayer mediaPlayer = new MediaPlayer(media);
		mediaView.setMediaPlayer(mediaPlayer);
		//mediaPlayer.play();
		if (index != 0 && index != 2 && index != 3) {
			mediaPlayer.setOnEndOfMedia(new Runnable() {
				public void run() {
					mediaPlayer.seek(Duration.ZERO);;
				}
			});
			mediaPlayer.setAutoPlay(true);
		}

	}
	public void pause(int index) {
		String s = null;
		try {
			s = getClass().getClassLoader().getResource(list.get(index)).toURI().toString();
		} catch (URISyntaxException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		Media media = new Media(s);
		MediaPlayer mediaPlayer = new MediaPlayer(media);
		mediaView.setMediaPlayer(mediaPlayer);
		mediaPlayer.pause();
	}

	public void updateLevel() {
		level++;
	}

}
