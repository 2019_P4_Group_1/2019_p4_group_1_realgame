package Map;
import sprites.Actor;
import sprites.Bullets;
import sprites.EnemyBullets;
import sprites.Mines;


import javafx.scene.image.Image;

public class Wall extends Actor{

	public Wall() {
		String path =  getClass().getClassLoader().getResource("resources/brick.gif").toString();
		Image image = new Image(path);
		this.setImage(image);
	}
	
	@Override
	public void act(long now) {
		
		if (this.getIntersectingObjects(Mines.class).size() > 0) {
			for (int i = 0; i < this.getIntersectingObjects(Mines.class).size(); i++) {
				if (this.getIntersectingObjects(Mines.class).get(i).getState() == 1) {
					getWorld().remove(this.getIntersectingObjects(Mines.class).get(i));
				}
			}
		}
		if(this.getIntersectingObjects(Bullets.class).size() > 0 || 
			this.getIntersectingObjects(EnemyBullets.class).size() > 0) {
			getWorld().remove(this);
		}

	}
	
}
