package Map;
import sprites.Actor;


import javafx.scene.image.Image;

public class Floor extends Actor {
	
	public Floor() {
		String path =  getClass().getClassLoader().getResource("resources/wooden_floor_background.jpg").toString();
		System.out.println(path);
		Image floor = new Image(path);
		this.setImage(floor);
	}
	
	
	
	@Override
	public void act(long now) {
		
	}

}

